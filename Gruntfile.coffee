exec = require("child_process").exec
path = require "path"
fs = require "fs"

###
# Grunt
###
module.exports = (grunt) ->
  grunt.loadNpmTasks "grunt-contrib-connect"
  
  grunt.initConfig
    connect:
      server:
        options:
          port: 9001
          hostname: "*"
          base: "."
          keepalive: true

  grunt.registerTask "default", ["connect"]